package com.turvo.service;

import com.turvo.configuration.exception.TurvoApplicationException;
import com.turvo.configuration.exception.TurvoApplicationExceptionCodes;
import com.turvo.domain.City;
import com.turvo.domain.Distance;
import com.turvo.repository.DistanceRepository;
import com.turvo.service.dto.DistanceDto;
import com.turvo.service.calculator.RouteCalculator;
import com.turvo.service.dto.RouteDto;
import com.turvo.service.impl.RouteServiceImpl;
import com.turvo.validator.PropertyValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.powermock.api.mockito.PowerMockito.doNothing;
import static org.powermock.api.mockito.PowerMockito.doThrow;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;

@RunWith(PowerMockRunner.class)
@PrepareForTest(RouteService.class)
public class RouteServiceTest {
  private PropertyValidator validator;
  private DistanceRepository distanceRepository;
  private RouteService routeService;

  @Before
  public void setUp() {
    validator = mock(PropertyValidator.class);
    distanceRepository = mock(DistanceRepository.class);
    routeService = new RouteServiceImpl(validator, distanceRepository);
  }

  @Test
  public void getRoutes_ValidCities_Success() throws Exception {
    // given
    Distance A_B =
        Distance.builder()
            .city1(City.builder().title("A").build())
            .city2(City.builder().title("B").build())
            .distance(200.0)
            .build();
    DistanceDto A_to_B_dto = DistanceDto.builder().city1("A").city2("B").distance(200.0).build();
    List<RouteDto> expected =
        Collections.singletonList(
            RouteDto.builder()
                .distanceDtos(Collections.singletonList(A_to_B_dto))
                .fullDistance(A_to_B_dto.getDistance())
                .build());

    RouteCalculator calculator = mock(RouteCalculator.class);
    when(calculator.calculate()).thenReturn(expected);
    whenNew(RouteCalculator.class).withAnyArguments().thenReturn(calculator);

    when(distanceRepository.getAll()).thenReturn(Collections.singletonList(A_B));
    when(distanceRepository.getByCity(anyString()))
        .thenReturn(Collections.singletonList(A_B));
    doNothing()
        .when(validator)
        .validateDuplicatedPropertyValue(anyString(), anyString(), anyString());

    // when
    List<RouteDto> actual = routeService.getRoutes("A", "B");

    // then
    assertEquals(expected, actual);
  }

  @Test(expected = TurvoApplicationException.class)
  public void getRoutes_NoRotues_Exception() throws Exception {
    // given
    RouteCalculator calculator = mock(RouteCalculator.class);
    when(calculator.calculate()).thenReturn(Collections.emptyList());
    whenNew(RouteCalculator.class).withAnyArguments().thenReturn(calculator);

    // when
    routeService.getRoutes("A", "B");
  }

  @Test(expected = TurvoApplicationException.class)
  public void getRoutes_DuplicatedCities_Exception() {
    // when
    doThrow(new TurvoApplicationException(TurvoApplicationExceptionCodes.DUPLICATED_PROPERTY_VALUE))
        .when(validator)
        .validateDuplicatedPropertyValue("finish", "A", "A");

    // then
    routeService.getRoutes("A", "A");
  }

  @Test(expected = TurvoApplicationException.class)
  public void getRoutes_TwoNotConnectedCities_Exception() {
    // given
    when(distanceRepository.getByCity(anyString())).thenReturn(Collections.emptyList());

    // when
    routeService.getRoutes("A", "B");
  }

  @Test(expected = TurvoApplicationException.class)
  public void getRoute_NotExistedStart_Exception() {
    // given
    when(distanceRepository.getByCity("NotExistedCity")).thenReturn(Collections.emptyList());

    // when
    routeService.getRoutes("NotExistedCity", "B");
  }

  @Test(expected = TurvoApplicationException.class)
  public void getRoute_NotExistedFinish_Exception() {
    // given
    when(distanceRepository.getByCity("NotExistedCity")).thenReturn(Collections.emptyList());

    // when
    routeService.getRoutes("A", "NotExistedCity");
  }
}
