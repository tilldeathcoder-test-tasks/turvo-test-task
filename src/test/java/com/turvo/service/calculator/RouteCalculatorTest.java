package com.turvo.service.calculator;

import com.turvo.service.dto.DistanceDto;
import com.turvo.service.dto.RouteDto;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RouteCalculatorTest {

  // First group of connected cities
  private DistanceDto A_to_B = DistanceDto.builder().city1("A").city2("B").distance(200.0).build();
  private DistanceDto A_to_E = DistanceDto.builder().city1("A").city2("E").distance(200.0).build();
  private DistanceDto B_to_E = DistanceDto.builder().city1("B").city2("E").distance(200.0).build();
  private DistanceDto B_to_C = DistanceDto.builder().city1("B").city2("C").distance(200.0).build();
  private DistanceDto E_to_F = DistanceDto.builder().city1("E").city2("F").distance(200.0).build();
  private DistanceDto C_to_F = DistanceDto.builder().city1("C").city2("F").distance(100.0).build();
  private DistanceDto F_to_I = DistanceDto.builder().city1("F").city2("I").distance(100.0).build();

  // Second group of connected cities. Two groups are not connected between each other!
  private DistanceDto D_to_G = DistanceDto.builder().city1("D").city2("G").distance(500.0).build();
  private DistanceDto H_to_G = DistanceDto.builder().city1("H").city2("G").distance(500.0).build();
  private DistanceDto H_to_D = DistanceDto.builder().city1("H").city2("D").distance(500.0).build();

  private List<DistanceDto> allCities =
      Arrays.asList(A_to_B, A_to_E, B_to_E, B_to_C, E_to_F, C_to_F, F_to_I, D_to_G, H_to_G, H_to_D);

  @Test
  public void calculate_FromAToIFourRoutes_Success() {
    // given
    List<RouteDto> expected =
        Arrays.asList(
            RouteDto.builder()
                .distanceDtos(Arrays.asList(A_to_B, B_to_E, E_to_F, F_to_I))
                .fullDistance(700.0)
                .build(),
            RouteDto.builder()
                .distanceDtos(Arrays.asList(A_to_B, B_to_C, C_to_F, F_to_I))
                .fullDistance(600.0)
                .build(),
            RouteDto.builder()
                .distanceDtos(Arrays.asList(A_to_E, B_to_E, B_to_C, C_to_F, F_to_I))
                .fullDistance(800.0)
                .build(),
            RouteDto.builder()
                .distanceDtos(Arrays.asList(A_to_E, E_to_F, F_to_I))
                .fullDistance(500.0)
                .build());

    // when
    RouteCalculator calculator = new RouteCalculator(allCities, "A", "I");
    List<RouteDto> actual = calculator.calculate();

    // then
    assertEquals(expected, actual);
  }

  @Test
  public void calculate_FromAToBThreeRoutes_Success() {
    // given
    List<RouteDto> expected =
        Arrays.asList(
            RouteDto.builder()
                .distanceDtos(Collections.singletonList(A_to_B))
                .fullDistance(200.0)
                .build(),
            RouteDto.builder()
                .distanceDtos(Arrays.asList(A_to_E, B_to_E))
                .fullDistance(400.0)
                .build(),
            RouteDto.builder()
                .distanceDtos(Arrays.asList(A_to_E, E_to_F, C_to_F, B_to_C))
                .fullDistance(700.0)
                .build());


    // when
    RouteCalculator calculator = new RouteCalculator(allCities, "A", "B");
    List<RouteDto> actual = calculator.calculate();

    // then
    assertEquals(expected, actual);
  }

  @Test
  public void calculate_FromAToDNoRoutes_Success() {
    // when
    RouteCalculator calculator = new RouteCalculator(allCities, "A", "D");
    List<RouteDto> actual = calculator.calculate();

    // then
    assertTrue(actual.isEmpty());
  }
}
