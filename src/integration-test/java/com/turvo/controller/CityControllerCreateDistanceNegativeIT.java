package com.turvo.controller;

import com.turvo.service.dto.DistanceDto;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;

public class CityControllerCreateDistanceNegativeIT extends BaseCityControllerIT {

  private void postDistanceValidateStatusCodeBeBadRequestAndMessage(Object body, String message) {
    given()
        .log()
        .all()
        .body(body)
        .contentType(ContentType.JSON)
        .when()
        .post(getDistancePath())
        .then()
        .log()
        .all()
        .statusCode(HttpStatus.SC_BAD_REQUEST)
        .body("message", containsString(message));
  }

  @Test
  public void createDistance_UnknownPropertyInRequestBody_Exception() {
    postDistanceValidateStatusCodeBeBadRequestAndMessage(
        "{\"city1\": \"A\",\"city2\": \"B\",\"distance\" : 2.0,\"unknown\": 2\"}",
        "JSON parse error: Unrecognized field \"unknown\"");
  }

  @Test
  public void createDistance_NullRequestBody_Exception() {
    postDistanceValidateStatusCodeBeBadRequestAndMessage("", "Required request body is missing: ");
  }

  @Test
  public void createDistance_PropertyStartIsEmpty_Exception() {
    postDistanceValidateStatusCodeBeBadRequestAndMessage(
        DistanceDto.builder().city1("").city2("Finish").distance(4.0).build(),
        "City1 can contain only letters and numbers.");
  }

  @Test
  public void createDistance_PropertyStartContainsPunctuationSymbol_Exception() {
    postDistanceValidateStatusCodeBeBadRequestAndMessage(
        DistanceDto.builder().city1("Start{}").city2("Finish").distance(4.0).build(),
        "City1 can contain only letters and numbers.");
  }

  @Test
  public void createDistance_PropertyFinishIsEmpty_Exception() {
    postDistanceValidateStatusCodeBeBadRequestAndMessage(
        DistanceDto.builder().city1("Start").city2("").distance(4.0).build(),
        "City2 can contain only letters and numbers.");
  }

  @Test
  public void createDistance_PropertyFinishIsEqualToStart_Exception() {
    postDistanceValidateStatusCodeBeBadRequestAndMessage(
        DistanceDto.builder().city1("Start").city2("Start").distance(4.0).build(),
        "Property can not be duplicated. Property - city2, value - Start.");
  }

  @Test
  public void createDistance_PropertyFinishContainsPunctuationSymbol_Exception() {
    postDistanceValidateStatusCodeBeBadRequestAndMessage(
        DistanceDto.builder().city1("Start").city2("Finish1!").distance(4.0).build(),
        "City2 can contain only letters and numbers.");
  }

  @Test
  public void createDistance_PropertyDistanceIsNull_Exception() {
    postDistanceValidateStatusCodeBeBadRequestAndMessage(
        DistanceDto.builder().city1("Start").city2("Finish").distance(null).build(),
        "Distance property can not be null.");
  }

  @Test
  public void createDistance_PropertyDistanceIsNegative_Exception() {
    postDistanceValidateStatusCodeBeBadRequestAndMessage(
        DistanceDto.builder().city1("Start").city2("Finish").distance(-4.1).build(),
        "Distance property can not be below zero.");
  }
}
