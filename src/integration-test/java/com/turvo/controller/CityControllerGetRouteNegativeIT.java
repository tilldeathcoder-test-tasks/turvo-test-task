package com.turvo.controller;

import com.turvo.service.dto.DistanceDto;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;

public class CityControllerGetRouteNegativeIT extends BaseCityControllerIT {

  private void getRouteValidateStatusCodeAndMessage(
      String start, String finish, int statusCode, String message) {
    given()
        .log()
        .all()
        .contentType(ContentType.JSON)
        .get(getRoutePath(start, finish))
        .then()
        .log()
        .all()
        .statusCode(statusCode)
        .body("message", equalTo(message));
  }

  @Test
  public void getRoute_NotConnected_Cities_Exception() {
    DistanceDto start = DistanceDto.builder().city1("A").city2("B").distance(200.0).build();
    DistanceDto notConnected = DistanceDto.builder().city1("C").city2("D").distance(200.0).build();
    testDBHelper.insert(start, notConnected);

    getRouteValidateStatusCodeAndMessage(
        "A", "C", HttpStatus.SC_BAD_REQUEST, "Router not found for start - A and finish - C.");
  }

  @Test
  public void getRoute_NotExistStartCity_Exception() {
    DistanceDto start = DistanceDto.builder().city1("A").city2("B").distance(200.0).build();
    DistanceDto finish = DistanceDto.builder().city1("B").city2("C").distance(200.0).build();
    testDBHelper.insert(start, finish);

    getRouteValidateStatusCodeAndMessage(
        "NotExist", "a", HttpStatus.SC_NOT_FOUND, "Entity was not found. Qualifier - NotExist.");
  }

  @Test
  public void getRoute_NotExistFinishCity_Exception() {
    DistanceDto start = DistanceDto.builder().city1("A").city2("B").distance(200.0).build();
    DistanceDto finish = DistanceDto.builder().city1("B").city2("C").distance(200.0).build();
    testDBHelper.insert(start, finish);

    getRouteValidateStatusCodeAndMessage(
        "A", "NotExist", HttpStatus.SC_NOT_FOUND, "Entity was not found. Qualifier - NotExist.");
  }

  @Test
  public void getRoute_FinishParameterEqualToStartParameter_Exception() {
    getRouteValidateStatusCodeAndMessage(
        "B",
        "B",
        HttpStatus.SC_BAD_REQUEST,
        "Property can not be duplicated. Property - finish, value - B.");
  }
}
