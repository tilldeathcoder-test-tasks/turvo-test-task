package com.turvo.controller;

import com.turvo.TurvoTestTaskApplication;
import com.turvo.configuration.TestDBHelper;
import io.restassured.RestAssured;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(
    classes = {TurvoTestTaskApplication.class},
    webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("dev")
public abstract class BaseCityControllerIT {

  protected static final String DISTANCE_PATH = "/cities/distance";
  protected static final String ROUTE_PATH_TEMPLATE = "%s/cities/route/%s/%s";

  @Autowired protected TestDBHelper testDBHelper;

  @Value("${server.servlet.context-path}")
  protected String basePath;

  @Value("${server.port}")
  private int port;

  @Before
  public void setUp() {
    testDBHelper.cleanDB();
    RestAssured.port = port;
  }

  protected String getDistancePath() {
    return basePath + DISTANCE_PATH;
  }

  protected String getRoutePath(String start, String finish) {
    return String.format(ROUTE_PATH_TEMPLATE, basePath, start, finish);
  }
}
