package com.turvo.controller;

import com.turvo.service.dto.DistanceDto;
import com.turvo.service.dto.RouteDto;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import org.apache.http.HttpStatus;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static io.restassured.RestAssured.given;

public class CityControllerGetRoutePositiveIT extends BaseCityControllerIT {

  private DistanceDto A_to_B = DistanceDto.builder().city1("A").city2("B").distance(200.0).build();
  private DistanceDto B_to_C = DistanceDto.builder().city1("B").city2("C").distance(200.0).build();
  private DistanceDto A_to_I = DistanceDto.builder().city1("A").city2("I").distance(200.0).build();
  private DistanceDto A_to_C = DistanceDto.builder().city1("A").city2("C").distance(200.0).build();
  private DistanceDto A_to_D = DistanceDto.builder().city1("A").city2("D").distance(200.0).build();
  private DistanceDto D_to_C = DistanceDto.builder().city1("D").city2("C").distance(100.0).build();
  private DistanceDto D_to_F = DistanceDto.builder().city1("D").city2("F").distance(100.0).build();
  private DistanceDto A_to_F = DistanceDto.builder().city1("A").city2("F").distance(500.0).build();
  private DistanceDto A_to_E = DistanceDto.builder().city1("A").city2("E").distance(500.0).build();
  private DistanceDto C_to_E = DistanceDto.builder().city1("C").city2("E").distance(50.0).build();
  private DistanceDto I_to_G = DistanceDto.builder().city1("I").city2("G").distance(100.0).build();
  private DistanceDto G_to_A = DistanceDto.builder().city1("G").city2("A").distance(100.0).build();

  private JsonPath getRouteValidateStatusCodeBeSuccess(String start, String finish) {
    return given()
        .log()
        .all()
        .contentType(ContentType.JSON)
        .get(getRoutePath(start, finish))
        .then()
        .log()
        .all()
        .statusCode(HttpStatus.SC_OK)
        .extract()
        .body()
        .jsonPath();
  }

  @Test
  public void getRoute_TwoConnectedCities_Success() {
    // given
    testDBHelper.insert(A_to_B);

    List<RouteDto> expected =
        Collections.singletonList(
            RouteDto.builder()
                .fullDistance(200.0)
                .distanceDtos(Collections.singletonList(A_to_B))
                .build());

    // when
    JsonPath response = getRouteValidateStatusCodeBeSuccess("A", "B");

    // then
    List<RouteDto> actual = response.getList("", RouteDto.class);
    Assert.assertEquals(expected, actual);
  }

  @Test
  public void getRoute_ThreeCitiesWithOnePath_Success() {
    // given
    testDBHelper.insert(A_to_B, B_to_C);

    List<RouteDto> expected =
        Collections.singletonList(
            RouteDto.builder()
                .fullDistance(400.0)
                .distanceDtos(Arrays.asList(A_to_B, B_to_C))
                .build());

    // when
    JsonPath response = getRouteValidateStatusCodeBeSuccess("A", "C");

    // then
    List<RouteDto> actual = response.getList("", RouteDto.class);
    Assert.assertEquals(expected, actual);
  }

  @Test
  public void getRoute_ThreeCitiesWithTwoPaths_Success() {
    // given
    testDBHelper.insert(A_to_B, B_to_C, A_to_C);

    List<RouteDto> expected =
        Arrays.asList(
            RouteDto.builder()
                .fullDistance(400.0)
                .distanceDtos(Arrays.asList(A_to_B, B_to_C))
                .build(),
            RouteDto.builder()
                .fullDistance(200.0)
                .distanceDtos(Collections.singletonList(A_to_C))
                .build());

    // when
    JsonPath response = getRouteValidateStatusCodeBeSuccess("A", "C");

    // then
    List<RouteDto> actual = response.getList("", RouteDto.class);
    Assert.assertEquals(expected, actual);
  }

  @Test
  public void getRoute_FourCitiesWithTwoPaths_Success() {
    // given
    testDBHelper.insert(A_to_B, B_to_C, A_to_C, A_to_D);

    List<RouteDto> expected =
        Arrays.asList(
            RouteDto.builder()
                .fullDistance(400.0)
                .distanceDtos(Arrays.asList(A_to_B, B_to_C))
                .build(),
            RouteDto.builder()
                .fullDistance(200.0)
                .distanceDtos(Collections.singletonList(A_to_C))
                .build());

    // when
    JsonPath response = getRouteValidateStatusCodeBeSuccess("A", "C");

    // then
    List<RouteDto> actual = response.getList("", RouteDto.class);
    Assert.assertEquals(expected, actual);
  }

  @Test
  public void getRoute_FourCitiesWithThreePaths_Success() {
    // given
    testDBHelper.insert(A_to_B, B_to_C, A_to_C, A_to_D, D_to_C);

    List<RouteDto> expected =
        Arrays.asList(
            RouteDto.builder()
                .fullDistance(400.0)
                .distanceDtos(Arrays.asList(A_to_B, B_to_C))
                .build(),
            RouteDto.builder()
                .fullDistance(200.0)
                .distanceDtos(Collections.singletonList(A_to_C))
                .build(),
            RouteDto.builder()
                .fullDistance(300.0)
                .distanceDtos(Arrays.asList(A_to_D, D_to_C))
                .build());

    // when
    JsonPath response = getRouteValidateStatusCodeBeSuccess("A", "C");

    // then
    List<RouteDto> actual = response.getList("", RouteDto.class);
    Assert.assertEquals(expected, actual);
  }

  @Test
  public void getRoute_FiveCitiesWithThreePaths_Success() {
    // given
    testDBHelper.insert(A_to_B, B_to_C, A_to_C, A_to_D, D_to_C, D_to_F);

    List<RouteDto> expected =
        Arrays.asList(
            RouteDto.builder()
                .fullDistance(400.0)
                .distanceDtos(Arrays.asList(A_to_B, B_to_C))
                .build(),
            RouteDto.builder()
                .fullDistance(200.0)
                .distanceDtos(Collections.singletonList(A_to_C))
                .build(),
            RouteDto.builder()
                .fullDistance(300.0)
                .distanceDtos(Arrays.asList(A_to_D, D_to_C))
                .build());

    // when
    JsonPath response = getRouteValidateStatusCodeBeSuccess("A", "C");

    // then
    List<RouteDto> actual = response.getList("", RouteDto.class);
    Assert.assertEquals(expected, actual);
  }

  @Test
  public void getRoute_FiveCitiesWithFourPaths_Success() {
    // given
    testDBHelper.insert(A_to_B, B_to_C, A_to_C, A_to_D, D_to_C, D_to_F, A_to_F);

    List<RouteDto> expected =
        Arrays.asList(
            RouteDto.builder()
                .fullDistance(400.0)
                .distanceDtos(Arrays.asList(A_to_B, B_to_C))
                .build(),
            RouteDto.builder()
                .fullDistance(200.0)
                .distanceDtos(Collections.singletonList(A_to_C))
                .build(),
            RouteDto.builder()
                .fullDistance(300.0)
                .distanceDtos(Arrays.asList(A_to_D, D_to_C))
                .build(),
            RouteDto.builder()
                .fullDistance(700.0)
                .distanceDtos(Arrays.asList(A_to_F, D_to_F, D_to_C))
                .build());

    // when
    JsonPath response = getRouteValidateStatusCodeBeSuccess("A", "C");

    // then
    List<RouteDto> actual = response.getList("", RouteDto.class);
    Assert.assertEquals(expected, actual);
  }

  @Test
  public void getRoute_SixCitiesWithFourPaths_Success() {
    // given
    testDBHelper.insert(A_to_B, B_to_C, A_to_C, A_to_D, D_to_C, D_to_F, A_to_F, A_to_E);

    List<RouteDto> expected =
        Arrays.asList(
            RouteDto.builder()
                .fullDistance(400.0)
                .distanceDtos(Arrays.asList(A_to_B, B_to_C))
                .build(),
            RouteDto.builder()
                .fullDistance(200.0)
                .distanceDtos(Collections.singletonList(A_to_C))
                .build(),
            RouteDto.builder()
                .fullDistance(300.0)
                .distanceDtos(Arrays.asList(A_to_D, D_to_C))
                .build(),
            RouteDto.builder()
                .fullDistance(700.0)
                .distanceDtos(Arrays.asList(A_to_F, D_to_F, D_to_C))
                .build());

    // when
    JsonPath response = getRouteValidateStatusCodeBeSuccess("A", "C");

    // then
    List<RouteDto> actual = response.getList("", RouteDto.class);
    Assert.assertEquals(expected, actual);
  }

  @Test
  public void getRoute_SixCitiesWithFivePaths_Success() {
    // given
    testDBHelper.insert(A_to_B, B_to_C, A_to_C, A_to_D, D_to_C, D_to_F, A_to_F, A_to_E, C_to_E);

    List<RouteDto> expected =
        Arrays.asList(
            RouteDto.builder()
                .fullDistance(400.0)
                .distanceDtos(Arrays.asList(A_to_B, B_to_C))
                .build(),
            RouteDto.builder()
                .fullDistance(200.0)
                .distanceDtos(Collections.singletonList(A_to_C))
                .build(),
            RouteDto.builder()
                .fullDistance(300.0)
                .distanceDtos(Arrays.asList(A_to_D, D_to_C))
                .build(),
            RouteDto.builder()
                .fullDistance(700.0)
                .distanceDtos(Arrays.asList(A_to_F, D_to_F, D_to_C))
                .build(),
            RouteDto.builder()
                .fullDistance(550.0)
                .distanceDtos(Arrays.asList(A_to_E, C_to_E))
                .build());

    // when
    JsonPath response = getRouteValidateStatusCodeBeSuccess("A", "C");
    // then
    List<RouteDto> actual = response.getList("", RouteDto.class);
    Assert.assertEquals(expected, actual);
  }

  @Test
  public void getRoute_SevenCitiesWithFivePaths_Success() {
    // given
    testDBHelper.insert(
        A_to_B, B_to_C, A_to_I, A_to_C, A_to_D, D_to_C, D_to_F, A_to_F, A_to_E, C_to_E);

    List<RouteDto> expected =
        Arrays.asList(
            RouteDto.builder()
                .fullDistance(400.0)
                .distanceDtos(Arrays.asList(A_to_B, B_to_C))
                .build(),
            RouteDto.builder()
                .fullDistance(200.0)
                .distanceDtos(Collections.singletonList(A_to_C))
                .build(),
            RouteDto.builder()
                .fullDistance(300.0)
                .distanceDtos(Arrays.asList(A_to_D, D_to_C))
                .build(),
            RouteDto.builder()
                .fullDistance(700.0)
                .distanceDtos(Arrays.asList(A_to_F, D_to_F, D_to_C))
                .build(),
            RouteDto.builder()
                .fullDistance(550.0)
                .distanceDtos(Arrays.asList(A_to_E, C_to_E))
                .build());

    // when
    JsonPath response = getRouteValidateStatusCodeBeSuccess("A", "C");

    // then
    List<RouteDto> actual = response.getList("", RouteDto.class);
    Assert.assertEquals(expected, actual);
  }

  @Test
  public void getRoute_EightCitiesWithFivePathsAndOneCycledNotConnectedPath_Success() {
    // given
    testDBHelper.insert(
        A_to_B, B_to_C, A_to_I, A_to_C, A_to_D, D_to_C, D_to_F, A_to_F, A_to_E, C_to_E, I_to_G,
        G_to_A);

    List<RouteDto> expected =
        Arrays.asList(
            RouteDto.builder()
                .fullDistance(400.0)
                .distanceDtos(Arrays.asList(A_to_B, B_to_C))
                .build(),
            RouteDto.builder()
                .fullDistance(200.0)
                .distanceDtos(Collections.singletonList(A_to_C))
                .build(),
            RouteDto.builder()
                .fullDistance(300.0)
                .distanceDtos(Arrays.asList(A_to_D, D_to_C))
                .build(),
            RouteDto.builder()
                .fullDistance(700.0)
                .distanceDtos(Arrays.asList(A_to_F, D_to_F, D_to_C))
                .build(),
            RouteDto.builder()
                .fullDistance(550.0)
                .distanceDtos(Arrays.asList(A_to_E, C_to_E))
                .build());

    // when
    JsonPath response = getRouteValidateStatusCodeBeSuccess("A", "C");

    // then
    List<RouteDto> actual = response.getList("", RouteDto.class);
    Assert.assertEquals(expected, actual);
  }
}
