package com.turvo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The entry point of the application.
 *
 * @author Yauhen.Makaranka
 */
@SpringBootApplication
public class TurvoTestTaskApplication {

  public static void main(String[] args) {
    SpringApplication.run(TurvoTestTaskApplication.class, args);
  }
}
