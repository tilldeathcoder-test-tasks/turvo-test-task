package com.turvo.validator;

import com.turvo.configuration.exception.TurvoApplicationException;
import com.turvo.configuration.exception.TurvoApplicationExceptionCodes;
import org.springframework.stereotype.Component;

/**
 * This class is used to validate different values for consistence.
 *
 * @author Yauhen.Makaranka
 */
@Component
public class PropertyValidator {

  /**
   * Validate string values be unique.
   *
   * @param propertyTitle - the property title
   * @param value1 - the first value
   * @param value2 - the second value
   * @throws TurvoApplicationException if not valid
   */
  public void validateDuplicatedPropertyValue(String propertyTitle, String value1, String value2) {
    if (value1.equalsIgnoreCase(value2)) {
      throw new TurvoApplicationException(
          TurvoApplicationExceptionCodes.DUPLICATED_PROPERTY_VALUE, propertyTitle, value2);
    }
  }
}
