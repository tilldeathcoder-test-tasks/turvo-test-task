package com.turvo.service;

import com.turvo.service.dto.RouteDto;

import java.util.List;

/**
 * The service class to work with {@link RouteDto} objects.
 *
 * @author Yauhen.Makaranka
 * @see RouteDto
 */
public interface RouteService {

  /**
   * Get route between two cities.
   *
   * @param start - the start point
   * @param finish - the finish point
   * @return the route between two cities
   */
  List<RouteDto> getRoutes(String start, String finish);
}
