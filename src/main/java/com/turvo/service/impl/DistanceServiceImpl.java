package com.turvo.service.impl;

import com.turvo.domain.City;
import com.turvo.domain.Distance;
import com.turvo.repository.DistanceRepository;
import com.turvo.service.DistanceService;
import com.turvo.service.dto.DistanceDto;
import com.turvo.validator.PropertyValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
public class DistanceServiceImpl implements DistanceService {

  private DistanceRepository distanceRepository;
  private PropertyValidator propertyValidator;

  @Autowired
  public DistanceServiceImpl(
      DistanceRepository distanceRepository, PropertyValidator propertyValidator) {
    this.distanceRepository = distanceRepository;
    this.propertyValidator = propertyValidator;
  }

  @Transactional
  @Override
  public void create(DistanceDto distanceDto) {
    log.info("Creating object: {}", distanceDto);
    propertyValidator.validateDuplicatedPropertyValue(
        "city2", distanceDto.getCity1(), distanceDto.getCity2());

    Distance newDistance =
        Distance.builder()
            .city1(City.builder().title(distanceDto.getCity1()).build())
            .city2(City.builder().title(distanceDto.getCity2()).build())
            .distance(distanceDto.getDistance())
            .build();

    this.distanceRepository.insert(newDistance);
  }
}
