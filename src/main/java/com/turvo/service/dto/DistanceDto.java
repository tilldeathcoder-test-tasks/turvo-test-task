package com.turvo.service.dto;

import com.turvo.domain.Distance;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;

/**
 * This class describes distance between two cities.
 *
 * @author Yauhen.Makaranka
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DistanceDto {

  @Pattern(regexp = "\\p{Alnum}+", message = "{city1_only_letters_and_numbers}")
  private String city1;

  @Pattern(regexp = "\\p{Alnum}+", message = "{city2_only_letters_and_numbers}")
  private String city2;

  @NotNull(message = "{distance_not_null}")
  @Positive(message = "{distance_not_negative}")
  private Double distance;

  public DistanceDto(Distance distance) {
    this.city1 = distance.getCity1().getTitle();
    this.city2 = distance.getCity2().getTitle();
    this.distance = distance.getDistance();
  }
}
