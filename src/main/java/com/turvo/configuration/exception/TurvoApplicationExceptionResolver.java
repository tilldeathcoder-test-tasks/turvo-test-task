package com.turvo.configuration.exception;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Locale;

/**
 * This class is used to resolve {@link TurvoApplicationException} code and parameters for readable
 * message and response status code.
 *
 * @author Yauhen.Makaranka
 * @see TurvoApplicationException
 * @see TurvoApplicationExceptionCodes
 */
@Component
public class TurvoApplicationExceptionResolver {

  private ReloadableResourceBundleMessageSource messageSource;

  @Autowired
  public TurvoApplicationExceptionResolver(ReloadableResourceBundleMessageSource messageSource) {
    this.messageSource = messageSource;
  }

  /**
   * This method resolves {@link TurvoApplicationException} code and its parameters for readable
   * message and response status code.
   *
   * @param exception - {@link TurvoApplicationException} exception object.
   * @return {@link ResponseExceptionInfo} object with resolved message and status code for
   *     response.
   */
  public ResponseEntity<ResponseExceptionInfo> resolve(TurvoApplicationException exception) {
    String message =
        messageSource.getMessage(
            exception.getInfo().getMessageKey(), exception.getParams(), null);
    ResponseExceptionInfo responseExceptionInfo =
        new ResponseExceptionInfo(message, LocalDateTime.now());
    return new ResponseEntity<>(responseExceptionInfo, exception.getInfo().getStatus());
  }
}
