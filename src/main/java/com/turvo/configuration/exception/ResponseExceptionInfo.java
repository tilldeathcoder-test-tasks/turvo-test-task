package com.turvo.configuration.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * This class describes information of exception that was thrown during request.
 *
 * @author Yauhen.Makaranka
 */
@Data
@AllArgsConstructor
public class ResponseExceptionInfo {

  private String message;
  private LocalDateTime dateTime;
}
