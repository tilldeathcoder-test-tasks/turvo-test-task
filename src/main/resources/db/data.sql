-- First group
insert into cities values (1, 'A');
insert into cities values (2, 'B');
insert into cities values (3, 'C');
insert into cities values (4, 'E');
insert into cities values (5, 'F');
insert into cities values (6, 'I');

-- A - B
insert into cities_distance values (1, 2, 3.0);
-- A - E
insert into cities_distance values (1, 4, 3.0);
-- B - C
insert into cities_distance values (2, 3, 4.0);
-- B - E
insert into cities_distance values (2, 4, 3.0);
-- F - C
insert into cities_distance values (5, 3, 3.0);
-- F - E
insert into cities_distance values (5, 4, 3.0);
-- F - I
insert into cities_distance values (5, 6, 3.0);

-- Second group. Second group and first group are not connected!
insert into cities values (7, 'D');
insert into cities values (8, 'H');
insert into cities values (9, 'G');

-- D - H
insert into cities_distance values (7, 8, 3.0);
-- H - G
insert into cities_distance values (8, 9, 4.0);
-- G - D
insert into cities_distance values (9, 7, 5.0);